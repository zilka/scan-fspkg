/* Copyright 2023 Roman Žilka

   This file is part of Scan-fs.

   Scan-fs is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 2 of the License, or (at your option) any later
   version.

   Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Scan-fs. If not, see <https://www.gnu.org/licenses/>. */



// All funcs except pkgdb_read_db() may only print something under pkgdb_debug
// or pkgdb_verbose.

#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>
#include "funcs-compat.h"
#include "pkgdb.h"

typedef struct {
	int err;
	array_ptr listing;
} lsdir_result_t;

const pkgdb_file pkgdb_file_blank = {.type=pkgdb_und};
// first chars of names must be unique
const char *const pkgdb_filetype_str[] = {[pkgdb_und]="und", [pkgdb_dir]="dir", [pkgdb_obj]="obj", [pkgdb_sym]="sym"};
const char *const pkgdb_filetype_struc[] = {[pkgdb_und]="Und", [pkgdb_dir]="Dir", [pkgdb_obj]="Obj", [pkgdb_sym]="Sym"};
bool pkgdb_verbose = false;
bool pkgdb_debug = false;

static array_ptr pkgdb_packages = {0};
// may not be pkgdb_free()d
pkgdb_file pkgdb_root = {.type=pkgdb_dir, .name="/", .namelen=1, .package=NULL, .ext.dir.contents=NULL};



static int file_namecmp(const void *i1, const void *i2) {
	assert(i1 && i2);

	int res;

	if (((const pkgdb_file *)i1)->namelen > ((const pkgdb_file *)i2)->namelen) {
		res = memcmp(((const pkgdb_file *)i1)->name, ((const pkgdb_file *)i2)->name, ((const pkgdb_file *)i2)->namelen);
		return res + ! res;
	}
	else if (((const pkgdb_file *)i1)->namelen < ((const pkgdb_file *)i2)->namelen) {
		res = memcmp(((const pkgdb_file *)i1)->name, ((const pkgdb_file *)i2)->name, ((const pkgdb_file *)i1)->namelen);
		return res - ! res;
	}
	else return memcmp(((const pkgdb_file *)i1)->name, ((const pkgdb_file *)i2)->name, ((const pkgdb_file *)i1)->namelen);
}



// doesn't touch .contents, .package
inline static void pkgdb_free(pkgdb_file file[static 1]) {
	if (file->type == pkgdb_sym)
		free(file->ext.sym.tgt);
	free(file->name);
	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=110694
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfree-nonheap-object"
	free(file);
#pragma GCC diagnostic pop
}



// Find the file with the path in the tree hierarchy under cwd. path must be
// fully canonical, except it may:
// - have sym in basename
// - have one trailing '/' => enforces type dir
// - be relative (both abs.+rel. paths are treated relatively to cwd)
// path="/" or empty => cwd.
const pkgdb_file *pkgdb_get(const pkgdb_file cwd[restrict static 1], const char path[restrict static 1]) {
	assert(cwd->type == pkgdb_dir);

	pkgdb_file key;
	key.name = (char *)path + (path[0]=='/');
	const void *vp;

	while (( key.namelen = strcspn(key.name, "/") )) {
		// .contents=NULL is ok for tfind()
		vp = tfind(&key, &cwd->ext.dir.contents, file_namecmp);
		key.name += key.namelen;
		if (vp) {
			cwd = *(const pkgdb_file **)vp;
			if (*key.name) {
				assert(*key.name == '/');
				if (cwd->type == pkgdb_dir)
					++key.name;
				else {
					errno = ENOTDIR;
					return NULL;
				}
			}
			else break;
		}
		else {
			errno = ENOENT;
			return NULL;
		}
	}

	return cwd;
}

const pkgdb_file *pkgdb_getn(const pkgdb_file cwd[restrict static 1], const char *restrict path, size_t pathlen) {
	assert(cwd->type == pkgdb_dir);

	const void *p;
	pkgdb_file key;
	key.name = (char *)path;
	if (pathlen && path[0]=='/') {
		--pathlen;
		++key.name;
	}

	while (pathlen) {
		assert(key.name[0] != '/');
		p = memchr(key.name, '/', pathlen);
		key.namelen = p ? (size_t)((const char *)p - key.name) : pathlen;
		// .contents=NULL is ok for tfind()
		p = tfind(&key, &cwd->ext.dir.contents, file_namecmp);
		key.name += key.namelen;
		pathlen -= key.namelen;
		if (p) {
			cwd = *(const pkgdb_file **)p;
			if (pathlen) {
				assert(*key.name == '/');
				if (cwd->type == pkgdb_dir) {
					++key.name;
					--pathlen;
				}
				else {
					errno = ENOTDIR;
					return NULL;
				}
			}
			else break;
		}
		else {
			errno = ENOENT;
			return NULL;
		}
	}

	return cwd;
}

const pkgdb_file *pkgdb_chdir(const pkgdb_file cwd[restrict static 1], const char path[restrict static 1]) {
	assert(cwd->type == pkgdb_dir);

	cwd = pkgdb_get(cwd, path);
	if (cwd && cwd->type != pkgdb_dir) {
		errno = ENOTDIR;
		return NULL;
	}
	return cwd;
}

const pkgdb_file *pkgdb_chdirn(const pkgdb_file cwd[restrict static 1], const char *restrict path, size_t pathlen) {
	assert(cwd->type == pkgdb_dir);

	cwd = pkgdb_getn(cwd, path, pathlen);
	if (cwd && cwd->type != pkgdb_dir) {
		errno = ENOTDIR;
		return NULL;
	}
	return cwd;
}



static void lsdir_twalk(const void *file, VISIT pass, void *out) {
	if (pass == postorder || pass == leaf) {  // in alphabetical order
		if ( ! ((lsdir_result_t *)out)->err &&
		     ! append_array_ptr(&((lsdir_result_t *)out)->listing, (const void **)(const pkgdb_file **)file, 1)
		   ) {
			((lsdir_result_t *)out)->err = errno;
		}
	}
}

// Returns array of ptrs to the contents (pkgdb_file structs) of dir. Sorted
// by .name (memcmp()-wise). Empty array => dir is empty
// (dir->ext.dir.contents=NULL), or error (otherwise).
array_ptr pkgdb_lsdir(const pkgdb_file dir[static 1]) {
	assert(dir->type == pkgdb_dir);

	lsdir_result_t result = {0};
	twalk_r(dir->ext.dir.contents, lsdir_twalk, &result);
	if (result.err) {
		free(result.listing.a);
		result.listing = array_ptr_empty;
		errno = result.err;
	}
	return result.listing;
}



// Removes file in the dir cwd. file is non-dir or an empty dir.
// cwd->ext.dir.contents (the ptr) may be updated.
void pkgdb_rm(pkgdb_file cwd[const restrict static 1], pkgdb_file file[restrict static 1]) {
	assert(cwd->type == pkgdb_dir && cwd->ext.dir.contents);
	assert(file->type != pkgdb_dir || ! file->ext.dir.contents);
	assert(tfind(file, &cwd->ext.dir.contents, file_namecmp));

	// tdelete() defines ret value for file-not-found and for file-found in a
	// non-root tree node. For file-found in the root tree node, the ret value
	// is unspecified, but it is the only circumstance under which *root
	// changes. All the while assuming root!=NULL initially.
	tdelete(file, &cwd->ext.dir.contents, file_namecmp);
	pkgdb_free(file);
}

static void rmcontents_recursively(void *node) {
	if ( ((pkgdb_file *)node)->type == pkgdb_dir &&
	     ((pkgdb_file *)node)->ext.dir.contents
	   ) {
		tdestroy(((pkgdb_file *)node)->ext.dir.contents, rmcontents_recursively);
	}
	pkgdb_free(node);
}

// recursively removes contents of dir, can be empty, dir itself is not removed
void pkgdb_rmcontents(pkgdb_file dir[const static 1]) {
	assert(dir->type == pkgdb_dir);

	tdestroy(dir->ext.dir.contents, rmcontents_recursively);
	dir->ext.dir.contents = NULL;
}



// TODO: PATH_MAX/NAME_MAX/SYMLINK_MAX check when pkgdb_debug
// file: whole struct loaded with final values, except:
// - .name contains full path (fully canonical), not necessarily \0-term., not
//   necessarily writable, ptr will be replaced, original one won't be freed
// - .namelen contains len of the interim .name
// - .ext.sym.tgt (in syms) doesn't have to be \0-term. or writable, will be
//   replaced with a copy, original ptr won't be freed
// The parent dir of file must already exist in the hierarchy under target
// (ENOENT).
//
// Places file under target, file's memory is used. If already present, file is
// freed. Returns ptr to file's position in the hierarchy under target.
// Error => nothing is modified or freed, returns NULL.
pkgdb_file *pkgdb_add(pkgdb_file target[const restrict static 1], pkgdb_file file[restrict static 1]) {
	// these are not a matter of pkgdb_debug
	assert(target->type == pkgdb_dir);
	assert(file->type && file->name && file->namelen && file->package);
	assert(file->type != pkgdb_dir || ! file->ext.dir.contents);
	assert(file->type != pkgdb_sym || file->ext.sym.tgt);
	assert(file->type != pkgdb_sym || file->ext.sym.tgtlen);

	if (pkgdb_debug) {
		// "//" in sym tgt isn't a concern
		if ( file->name[0] != '/' || ! file->name[1] ||
		     hasdotdirn(file->name, file->namelen) ||
		     memmem(file->name, file->namelen, "//", 2) ||
		     file->name[file->namelen-1] == '/'
		   ) {
			errno = EINVAL;
			return NULL;
		}
	}

	const char *basename = memrchr(file->name, '/', file->namelen);
	pkgdb_file *pkgdbf = (pkgdb_file *)pkgdb_chdirn(target, file->name, basename-file->name);
	if (! pkgdbf)
		return NULL;

	int err = 0;
	void *p;
	char *const name_orig = file->name;
	const size_t namelen_orig = file->namelen;
	char *const tgt_orig = file->ext.sym.tgt;
	if (file->type == pkgdb_sym)
		file->ext.sym.tgt = NULL;
	file->namelen -= ++basename - file->name;
	if ( ! (file->name = strexndup(file->namelen, basename)) ||
	     ( file->type == pkgdb_sym &&
	       ! (file->ext.sym.tgt = strexndup(file->ext.sym.tgtlen, tgt_orig))
	     )
	   )
		err = errno;
	else if (! (p = tsearch(file, &pkgdbf->ext.dir.contents, file_namecmp)))
		// tsearch() doesn't set errno, ENOMEM is the only possibility in this
		// case
		err = ENOMEM;
	else {
		pkgdbf = *(pkgdb_file **)p;

		// Not found + added .contents of the containing dir may have been
		// updated by tsearch().
		if (pkgdbf == file)
			return pkgdbf;

		// a file with the same dir+name already recorded
		// there are differing properties
		if ( pkgdb_debug &&
		     // pkgdbf->ext.dir.contents and .package don't come into this
		     ( pkgdbf->type != file->type ||
		       ( file->type == pkgdb_obj &&
		         ( ! md5_equal(pkgdbf->ext.obj.hash, file->ext.obj.hash) ||
		           pkgdbf->ext.obj.timestamp != file->ext.obj.timestamp
		         )
		       ) ||
		       ( file->type == pkgdb_sym &&
		         ( pkgdbf->ext.sym.tgtlen != file->ext.sym.tgtlen ||
		           memcmp(pkgdbf->ext.sym.tgt, file->ext.sym.tgt, file->ext.sym.tgtlen) ||
		           pkgdbf->ext.sym.timestamp != file->ext.sym.timestamp
		         )
		       )
		     )
		   ) {
			err = EEXIST;
		}
		// there are no differing properties (or we don't care)
		else {
			pkgdb_free(file);
			return pkgdbf;
		}
	}

	free(file->name);
	if (file->type == pkgdb_sym) {
		free(file->ext.sym.tgt);
		file->ext.sym.tgt = tgt_orig;
	}
	file->name = name_orig;
	file->namelen = namelen_orig;
	errno = err;
	return NULL;
}



// Result satisfies reqs of pkgdb_add(), except .package is uninitialized. line
// is \0-term, its length is len. Only syntactic checks here.
//
// In the pkg db:
// 1. the dirname of every path is canonical and without symlinks
// 2. for every file in a pkg the pkg also owns its parent dir, which is listed
//    earlier in CONTENTS
// 3. path is fully canonical
static pkgdb_file *parse_contents_line(const char line[static 1], size_t len) {
	pkgdb_file *file = malloc(sizeof (pkgdb_file));
	if (! file)
		return NULL;

	const char *s;
	uintmax_t tstamp;
	int err = 0;

	switch (line[0]) {
	// obj /bin/arping db9fa5c75077f93ea359928acfdfa079 1551473055
	case 'o':
		if (pkgdb_debug) {
			if ( strncmp(line, "obj ", 4) || ! *(file->name=(char *)line+4) ||
			     *file->name == ' ' ||
			     ! (line=memrchr(file->name,' ',len-4)) || *++line < '0' ||
			     *line > '9' || *(line+strspn(line,"0123456789")) ||
			     ! (s=memrchr(file->name,' ',(line-1)-file->name)) ||
			     strspn(++s,"0123456789abcdef") != PKGDB_HASHLEN ||
			     s + (PKGDB_HASHLEN+1) != line ||
			     (tstamp=strtoumax(line,NULL,10)) > TIME_MAX
			   ) {
				err = EINVAL;
				break;
			}
		}
		else {
			file->name = (char *)line + 4;
			line = (char *)memrchr(line, ' ', len) + 1;
			s = line - (1 + PKGDB_HASHLEN);
			tstamp = strtoumax(line, NULL, 10);
		}
		// ok, path: file->name, hash: s, tstamp: line
		file->namelen = (s-1) - file->name;
		file->ext.obj.timestamp = tstamp;
		md5_str2hash(file->ext.obj.hash, s);
		file->type = pkgdb_obj;
		break;

	// dir /bin
	case 'd':
		if (pkgdb_debug) {
			if ( strncmp(line, "dir ", 4) || ! *(file->name=(char *)line+4) ||
			     *file->name == ' '
			   ) {
				err = EINVAL;
				break;
			}
		}
		else file->name = (char *)line + 4;
		// ok, path: file->name
		file->namelen = len - 4;
		file->ext.dir.contents = NULL;
		file->type = pkgdb_dir;
		break;

	// sym /bin/bb -> busybox 1551466447
	case 's':
		if (pkgdb_debug) {
			// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108154
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstringop-overread"
			if ( strncmp(line, "sym ", 4) || ! *(file->name=(char *)line+4) ||
			     *file->name == ' ' ||
			     ! (file->ext.sym.tgt=strstr(file->name," -> ")) ||
			     ! *(file->ext.sym.tgt+=4) || *file->ext.sym.tgt == ' ' ||
			     ! (s=memrchr(file->ext.sym.tgt,' ',len-(file->ext.sym.tgt-line))) ||
			      *++s < '0' || *s > '9' || *(s+strspn(s,"0123456789")) ||
			     (tstamp=strtoumax(s,NULL,10)) > TIME_MAX
			   ) {
				err = EINVAL;
				break;
			}
		}
		else {
			file->name = (char *)line + 4;
			file->ext.sym.tgt = strstr(file->name, " -> ") + 4;
#pragma GCC diagnostic pop
			s = (char *)memrchr(line, ' ', len) + 1;
			tstamp = strtoumax(s, NULL, 10);
		}
		// ok, path: file->name, target: file->ext.sym.tgt, tstamp: s
		file->namelen = (file->ext.sym.tgt-4) - file->name;
		file->ext.sym.tgtlen = (s-1) - file->ext.sym.tgt;
		file->ext.sym.timestamp = tstamp;
		file->type = pkgdb_sym;
		break;
	}

	if (err) {
		free(file);
		errno = err;
		return NULL;
	}
	return file;
}



// Populates pkgdb_root and pkgdb_packages, replacing their previous contents.
// Failure => pkgdb_root and pkgdb_packages empty. Doesn't set errno.
pkgdb_file *pkgdb_read_db(void) {
	assert(pkgdb_root.type == pkgdb_dir);
	assert(! strcmp(pkgdb_root.name, "/"));
	assert(pkgdb_root.namelen == 1);

	pkgdb_file *file;
	array_char path = array_char_empty;
	char *package, *line = NULL;
	size_t dir1namelen, dir2namelen, linesize = 0;
	DIR *dir1handle, *dir2handle;
	struct dirent *direntry;
	ssize_t l;
	FILE *f;
	bool ok = true;

	if (pkgdb_packages.len) {
		for (void **p=pkgdb_packages.a; p<pkgdb_packages.a+pkgdb_packages.len; ++p)
			free(*p);
		free(pkgdb_packages.a);
		pkgdb_packages = array_ptr_empty;
	}
	ensalloc_array_ptr(&pkgdb_packages, 1024);
	pkgdb_rmcontents(&pkgdb_root);

	// this section could be greatly shortened with glob(), but that has unclear
	// multi-threading safety
	do {
		// path = /var/db/pkg/
		if ( ! copy_array_char(&path, PKGDB_LOCATION, strlen(PKGDB_LOCATION)) ||
		     ! append_array_char(&path, "/", 1) ||
		     ! zeroterm_array_char(&path)  // opendir()
		   ) {
			error(0, errno, "%s()", __func__);
			ok = false;
			break;
		}
		dir1handle = opendir(path.a);
		if (! dir1handle) {
			error(0, errno, "opendir(%s)", path.a);
			ok = false;
			break;
		}
		dir1namelen = path.len;

		while ( (errno = 0, direntry = readdir(dir1handle)) ||
		        (errno && (error(0,errno,"readdir(%s)",path.a), ok=false))
		      ) {
			if (isdotdir(direntry->d_name))
				continue;

			// path = /var/db/pkg/sec-tion/
			if ( ! append_array_char(&path, direntry->d_name, strlen(direntry->d_name)) ||
			     ! append_array_char(&path, "/", 1) ||
			     ! zeroterm_array_char(&path)  // opendir()
			   ) {
				error(0, errno, "%s()", __func__);
				ok = false;
				break;
			}
			dir2handle = opendir(path.a);
			if (! dir2handle) {
				error(0, errno, "opendir(%s)", path.a);
				ok = false;
				break;
			}
			dir2namelen = path.len;

			while ( (errno = 0, direntry = readdir(dir2handle)) ||
			        (errno && (error(0,errno,"readdir(%s)",path.a), ok=false))
			      ) {
				if (isdotdir(direntry->d_name))
					continue;

				// path = /var/db/pkg/sec-tion/package-1.0/CONTENTS
				if ( ! append_array_char(&path, direntry->d_name, strlen(direntry->d_name)) ||
				     ! append_array_char(&path, "/CONTENTS", 9) ||
				     ! zeroterm_array_char(&path)  // fopen()
				   ) {
					ok = false;
					error(0, errno, "%s()", __func__);
					break;
				}
				// do package before fopen() because of fclose()
				package = strexndup(path.len-dir1namelen-9, path.a+dir1namelen);
				if ( ! package ||
				     ! append_array_ptr(&pkgdb_packages, (const void **)&package, 1)
				   ) {
					ok = false;
					error(0, errno, "%s()", __func__);
					free(package);
					break;
				}
				if (pkgdb_verbose)
					puts(path.a);
				f = fopen(path.a, "r");
				if (! f) {
					ok = false;
					error(0, errno, "fopen(%s)", path.a);
					break;
				}
				setvbuf(f, NULL, _IOFBF, IOSIZE);

				for (;;) {
					// if path.a is a dir, getline() fails with EISDIR
					l = getline(&line, &linesize, f);
					if (l == -1) {
						int e = errno;
						// ferror() is unreliable (see funcs-compat.c)
						if (! feof(f)) {
							ok = false;
							error(0, e, "getline(%s)", path.a);
						}
						break;
					}
					if (pkgdb_debug) {
						if (strlen(line) != (size_t)l) {
							// parse_contents_line() and perhaps others rely on
							// this (?)
							ok = false;
							error(0, 0, "%s: NUL byte found in line %.20s", path.a, line);
							break;
						}
					}
					assert(l > 0);
					if (line[l-1] == '\n')
						line[l---1] = '\0';
					file = parse_contents_line(line, l);
					if (! file) {
						ok = false;
						error(0, errno, "%s: parse_contents_line(%.20s)", path.a, line);
						break;
					}
					if (pkgdb_verbose) {
						char md5[PKGDB_HASHLEN];
						switch (file->type) {
						case pkgdb_dir:
							printf("%s %.*s\n", pkgdb_filetype_str[file->type], cutint(file->namelen), file->name);
							break;
						case pkgdb_obj:
							md5_hash2str(md5, file->ext.obj.hash, false);
							printf("%s %.*s %.*s %ju\n", pkgdb_filetype_str[file->type], cutint(file->namelen), file->name, (int)PKGDB_HASHLEN, md5, (uintmax_t)file->ext.obj.timestamp);
							break;
						case pkgdb_sym:
							printf("%s %.*s -> %.*s %ju\n", pkgdb_filetype_str[file->type], cutint(file->namelen), file->name, cutint(file->ext.sym.tgtlen), file->ext.sym.tgt, (uintmax_t)file->ext.sym.timestamp);
							break;
						case pkgdb_und:
							assert(file->type);
							break;
						}
					}
					file->package = package;
					if (! pkgdb_add(&pkgdb_root, file)) {
						ok = false;
						error(0, errno, "pkgdb_add(%.*s)", cutint(file->namelen), file->name);
						free(file);
						break;
					}
				}

				if (fclose(f)) {
					ok = false;
					error(0, errno, "fclose(%s)", path.a);
				}
				path.len = dir2namelen;
			}

			closedir(dir2handle);
			if (! ok)
				break;
			path.len = dir1namelen;
		}

		closedir(dir1handle);
	} while (0);

	free(path.a);
	free(line);
	if (! ok) {
		for (void **p=pkgdb_packages.a; p<pkgdb_packages.a+pkgdb_packages.len; ++p)
			free(*p);
		free(pkgdb_packages.a);
		pkgdb_packages = array_ptr_empty;
		pkgdb_rmcontents(&pkgdb_root);
		return NULL;
	}
	return &pkgdb_root;
}
