/* Copyright 2023 Roman Žilka

   This file is part of Scan-fs.

   Scan-fs is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 2 of the License, or (at your option) any later
   version.

   Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Scan-fs. If not, see <https://www.gnu.org/licenses/>. */



#include "funcs.h"
#include "denylist.h"

#define TYPE struct denylist_entry
#define TYPESHORT dlentry
#define ARRAY_CUSTOM1
#include "array.c"

// entries are never freed, there may be freeable and non-freeable paths in it
static array_dlentry denylist = {0};



// a trailing '/' in s overrides and implies s_known_to_be_dir=true
bool denylisted(const char s[static 1], size_t len, bool s_known_to_be_dir) {
	assert(s[0] == '/');
	assert(! hasdotdirn(s, len));
	assert(! memmem(s, len, "//", 2));

	for (size_t n=0; n<denylist.len; ++n) {
		if ( len >= denylist.a[n].pathlen &&
		     ! memcmp(denylist.a[n].path, s, denylist.a[n].pathlen) &&
		     ( len == denylist.a[n].pathlen ||
		       s[denylist.a[n].pathlen] == '/' ||
		       denylist.a[n].pathlen == 1  // denylist.a[n].path="/"
		     ) &&
		     (! denylist.a[n].isdir || s_known_to_be_dir || s[len-1] == '/')
		   ) {
			return true;
		}
	}
	return false;
}



// TODO: prune denylist if s denylists something on it
// Insert while keeping it sorted. s is \0-term.
bool denylist_addordiscard(char s[static 2], bool freeable) {
	assert(s[0] == '/');
	assert(! hasdotdir(s));
	assert(! strstr(s, "//"));

	size_t slen = strlen(s);
	bool sisdir = false;
	if (s[slen-1] == '/') {
		sisdir = true;
		if (slen > 1)
			--slen;
	}

	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=110694
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfree-nonheap-object"
	if (denylisted(s, slen, sisdir)) {
		if (freeable)
			free(s);
		return true;
	}

	size_t n;
	int i;
	for (n=0; n<denylist.len; ++n) {
		i = memcmp(denylist.a[n].path, s, denylist.a[n].pathlen < slen ? denylist.a[n].pathlen : slen);
		if (! i && denylist.a[n].pathlen == slen) {
			assert(! sisdir && denylist.a[n].isdir);
			denylist.a[n].isdir = false;
			if (freeable)
				free(s);
			return true;
		}
		if (i > 0 || (! i && denylist.a[n].pathlen > slen))
			break;
	}

	if (! insert_array_dlentry(&denylist, n+1, 1)) {
		if (freeable)
			free(s);
		return false;
	}
#pragma GCC diagnostic pop

	denylist.a[n].path = s;
	denylist.a[n].pathlen = slen;
	denylist.a[n].isdir = sisdir;
	denylist.a[n].freeable = freeable;
	return true;
}



void denylist_erase(void) {
	for (size_t i=0; i<denylist.len; ++i) {
		if (denylist.a[i].freeable)
			free(denylist.a[i].path);
	}
	free(denylist.a);
	denylist = array_dlentry_empty;
}



/*
// Sort \0-term paths. No double '/'s, no "."/".."s. "some/path/" > "some/path".
static inline int path_strcmp(const void *s1, const void *s2) {
	assert(s1 && s2);

	int i;
	size_t s1l, s2l, curlen;
	const char *s11=s1, *s22=s2;
	for (;;) {
		s1l = strcspn(s11, "/");
		s2l = strcspn(s22, "/");
		curlen = s1l<=s2l ? s1l : s2l;
		i = memcmp(s11, s22, curlen);
		if (i)
			return i;
		if (s1l != s2l)
			return s11[curlen]=='\0' || s11[curlen]=='/' ? -1 : 1;
		s11 += curlen;
		s22 += curlen;
		if (*s11 == '\0')
			return (bool)*s22;
		if (*s22 == '\0')
			return 1;
		++s11; ++s22;
	}
}
*/

/*
static inline int qsort_strcmp(const void *s1, const void *s2) {
	assert(s1 && s2);
	return strcmp(s1, s2);
}
*/

/*
// NULL > any other ptr
static int denylist_qsort(const void *s1, const void *s2) {
	if (! *(const char **)s1)
		return *(const char **)s2 != NULL;
	if (! *(const char **)s2)
		return -1;
	return strcmp(*(const char **)s1, *(const char **)s2);
}

// prepares denylist for use by denylisted(), removes redundant entries
// initializes the denylist_lens array
// ptrs in denylist can be NULL
// if freeunused==true, free()s the redundant denylist entries
// returns success status, preserves errno
// After a return with false:
// - denylist still has `nr' entries, each of which is either one of the
//   original pointers or NULL, the NULLed pointers had been freed (if
//   freeunused==true, that is)
// - denylist_lens is free()able (NULL or allocated)
bool finalize_denylist(size_t nr, bool freeunused) {
	char *s;
	void *p;
	size_t cur, i, slen;

	if (nr) {
		qsort(denylist, nr, sizeof(*denylist), denylist_qsort);
		while (nr && ! denylist[nr-1])
			--nr;

		// do this now - required by denylisted()
		if (nr) {
			// nr must be size_t
			if (! fitprod2zuzu(sizeof(*denylist_lens), nr))
				return false;
			denylist_lens = malloc(nr * sizeof(*denylist_lens));
			if (! denylist_lens)
				return false;

			// trim trailing / from denylist entries and pre-compute their lens
			// denylist_lens isn't NULL-term.
			for (cur=0; cur<nr; ++cur) {
				i = strlen(denylist[cur]);
				if (i > 1 && denylist[cur][i-1] == '/')
					denylist[cur][i---1] = '\0';
				denylist_lens[cur] = i;
			}
		}

		// eliminate those denylist entries which are denylisted by the rest
		for (cur=nr; cur; --cur) {
			// must be sorted now
			s = denylist[cur-1];
			slen = denylist_lens[cur-1];
			memmove(denylist+(cur-1), denylist+(cur), (nr-cur)*sizeof(*denylist));
			memmove(denylist_lens+(cur-1), denylist_lens+cur, (nr-cur)*sizeof(*denylist_lens));
			denylist[nr-1] = NULL;
			// redundant -e param.?
			if (denylisted(s)) {
				if (freeunused)
					free(s);
				--nr;
			}
			else {
				memmove(denylist+cur, denylist+(cur-1), (nr-cur)*sizeof(*denylist));
				memmove(denylist_lens+cur, denylist_lens+(cur-1), (nr-cur)*sizeof(*denylist_lens));
				denylist[cur-1] = s;
				denylist_lens[cur-1] = slen;
			}
		}

		if (denylist_lens) {
			if (nr) {
				p = realloc(denylist_lens, nr*sizeof(*denylist_lens));
				if (! p)
					return false;
				denylist_lens = p;
			}
			else {
				free(denylist_lens);
				denylist_lens = NULL;
			}
		}
	}

	if (! fit2zuzu(nr*sizeof(*denylist), sizeof(*denylist)))
		return false;
	p = realloc(denylist, (nr+1)*sizeof(*denylist));
	if (! p)
		return false;
	denylist = p;
	denylist[nr] = NULL;

	return true;
}
*/
