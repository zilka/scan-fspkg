/* Copyright 2023 Roman Žilka

   This file is part of Scan-fs.

   Scan-fs is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 2 of the License, or (at your option) any later
   version.

   Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Scan-fs. If not, see <https://www.gnu.org/licenses/>. */



#ifndef PKGDB_H_INCLUDED
#define PKGDB_H_INCLUDED

#include "funcs.h"

#define PKGDB_LOCATION "/var/db/pkg"
#define PKGDB_HASHLEN ((size_t)32)

// und = undefined = 0
typedef enum {pkgdb_und, pkgdb_dir, pkgdb_obj, pkgdb_sym} pkgdb_filetype;

// TODO: .package only covers one owner
// TODO: include package in output wherever relevant
// All strings !=NULL, \0-term., the lens don't include it. Non-const ptrs own
// data.
// Don't 0-initialize. When zero-initializing the union, only its first member
// is set to 0. When it's the largest one, the whole area of the union will get
// initialized. But due to the variable length of the types involved, the
// largest member can't be determined here.
typedef struct {
	pkgdb_filetype type;
	char *name;
	size_t namelen;
	const char *package;
	union {
		struct {
			// -> root of a search tree (tsearch()), NULL <==> dir is empty
			void *contents;
		} dir;
		struct {
			md5_arith_t hash[4];
			time_t timestamp;
		} obj;
		struct {
			char *tgt;
			size_t tgtlen;
			time_t timestamp;
		} sym;
	} ext;
} pkgdb_file;

extern const pkgdb_file pkgdb_file_blank;
extern const char *const pkgdb_filetype_str[];
extern const char *const pkgdb_filetype_struc[];
extern bool pkgdb_verbose;
extern bool pkgdb_debug;

// this is the pkg db, initd to "/", no pkg owns it
extern pkgdb_file pkgdb_root;



// R/O functions, thread-safe
// find the file with path, relative to base dir cwd
const pkgdb_file *pkgdb_get(const pkgdb_file cwd[restrict static 1], const char path[restrict static 1]);
const pkgdb_file *pkgdb_getn(const pkgdb_file cwd[restrict static 1], const char *restrict path, size_t pathlen);
// like get(), but path must be dir
const pkgdb_file *pkgdb_chdir(const pkgdb_file cwd[restrict static 1], const char path[restrict static 1]);
const pkgdb_file *pkgdb_chdirn(const pkgdb_file cwd[restrict static 1], const char *restrict path, size_t pathlen);
// ls of a dir contents
array_ptr pkgdb_lsdir(const pkgdb_file dir[static 1]);

// R/W functions, not thread-safe
// remove file in cwd
void pkgdb_rm(pkgdb_file cwd[const restrict static 1], pkgdb_file file[restrict static 1]);
// recursively remove contents of dir
void pkgdb_rmcontents(pkgdb_file dir[const static 1]);
// add new file into the dir tree, file.name = full path
pkgdb_file *pkgdb_add(pkgdb_file target[const restrict static 1], pkgdb_file file[restrict static 1]);
// reads pkg db from disk, filling pkgdb_root
pkgdb_file *pkgdb_read_db(void);

#endif
