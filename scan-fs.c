/* Copyright 2023 Roman Žilka

   This file is part of Scan-fs.

   Scan-fs is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 2 of the License, or (at your option) any later
   version.

   Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Scan-fs. If not, see <https://www.gnu.org/licenses/>. */



// TODO: something like cut -d -f? for, e.g., parse_contents_line(), paths, ...
// TODO(?): check_if_path_is_fully_canonical(const char*)
// TODO(?): pkgsonly doesn't need the complete prior read-up of the pkgdb
// TODO: make canonicalize_dirname() on cmdline an option
// TODO: look for suspicious files (UID/GID without a name, dead symlinks, ...)
// TODO: deal with the "//" path prefix! See POSIX: General Concepts -> Pathname
//       Resolution (very bottom)
// TODO: when interrupted by a signal, most stdio funcs can't tell how much they
//       could output, correction is impossible
// TODO: what does 'equery b' do when a sym points to another sym? '-f' should
//       emulate it.
// TODO: custom versions of string.h funcs that handle NULL for 0 size

#include <sys/stat.h>
#include <assert.h>
#include <dirent.h>
#include <locale.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include "assumptions.h"
#include "funcs-compat.h"
#include "pkgdb.h"
#include "denylist.h"

#ifndef BUILDSTAT
#define BUILDSTAT (unknown build)
#endif

// POSIX: EXIT_SUCCESS is defined as 0, EXIT_FAILURE can be anything
#define EXIT_SOMERESULTS 1
#define EXIT_USERERROR 2
#define EXIT_PROGERROR 3
static uint8_t exitcode = EXIT_SUCCESS;  // no errors, empty result set

static struct {
	size_t nrthreads;
	bool verifymeta;
	bool samefs;
	bool verbose;
	bool tryfollow;
	bool allmissing;
} scanfs_args = {.nrthreads=1};

// missing = file missing, ok = file exists and metadata known,
// unknown = existence of file and metadata unknowable
typedef enum {ondisk_missing, ondisk_ok, ondisk_unknown} ondisk_status;
typedef struct {
	array_char path;  // \0-term.
	struct stat meta;  // only initialized when .ondisk=ondisk_ok
	const pkgdb_file *pkgdbf;  // NULL <=> path not in pkgdb
	ondisk_status ondisk;
	bool oneshot;  // true => don't descend into path if it is a dir
} job;
// Making jobstack array of job would save an alloc+free per every job, but
// copying of the entire job contents (incl. .path) would have to happen in a
// critical section and job_get() would have to search jobstack for a job that's
// not currently being worked on (jobs would have to be marked to that end).
static array_ptr jobstack = {0};  // (job *)[]
static size_t busythreads = 0;
static pthread_mutex_t jobsmx;  // jobstack, busythreads
static pthread_cond_t jobsevent;  // ++jobstack, --busythreads

#define IFEX(condition) do { if (condition) { \
	                        error(0, errno, "%s()", __func__); \
	                        flush_exit(mergeexits(exitcode, EXIT_PROGERROR)); \
                        } } while (0)



static inline uint8_t mergeexits(uint8_t ex1, uint8_t ex2) {
	if (ex1 == EXIT_USERERROR || ex2 == EXIT_USERERROR)
		return EXIT_USERERROR;
	if (ex1 == EXIT_PROGERROR || ex2 == EXIT_PROGERROR)
		return EXIT_PROGERROR;
	if (ex1 == EXIT_SUCCESS)
		return ex2;
	return EXIT_SOMERESULTS;
}



// TODO: checks for stdio funcs
// from exit(): no way to notify of fflush(stderr) failure, no way to change
// exit status
static void stdio_flush_last(void) {
	while (fflush(stdout)) {
		if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
			error(0, errno, "fflush(stdout)");
			break;
		}
	}
	while (fflush(stderr)) {  // stderr last because of the previous error()
		if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK)
			break;
	}
}

static void flush_exit(uint8_t code) {
	while (fflush(stdout)) {
		if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
			error(0, errno, "fflush(stdout)");
			code = mergeexits(code, EXIT_PROGERROR);
			break;
		}
	}
	while (fflush(stderr)) {
		if (errno != EINTR && errno != EAGAIN && errno != EWOULDBLOCK) {
			// might still get through somehow in stdio_flush_last()
			error(0, errno, "fflush(stderr)");
			code = mergeexits(code, EXIT_PROGERROR);
			break;
		}
	}

	exit(code);
}



// '-f' is what 'equery b' does. Any symlink which points to something that's
// owned by a package is automatically owned. Kinda wrong, if you ask me.
static void usage(uint8_t code, size_t maxthreads) {
	FILE *const out = code ? stderr : stdout;
	int i;

	fprintf(out, "scan-fs " STRINGIZE_EXP(BUILDSTAT) "\n"
	             "\n");
	fprintf(out, "Usage: %s [-e path]... [-xfmatvdh] /path/to/scan [PATH_TO_SCAN]...\n"
	             "       %s -h\n"
	             "\n", program_invocation_short_name, program_invocation_short_name);
	fprintf(out, "Options: -e /path/to/exclude don't scan this path; repeatable\n"
	             "         -x                  stay within the filesystem the requested path is\n"
	             "                             on; also trims pkg db search\n"
	             "         -f                  if a symlink on disk is missing in pkg db, check\n"
	             "                             its target instead; ignores -x and -e for the\n"
	             "                             target; if target is a dir, its contents are not\n"
	             "                             scanned\n"
	             "         -m                  compare metadata (hash, mtime, symlink target)\n"
	             "         -a                  scan contents of on-disk dirs missing in pkg db (or\n"
	             "                             vice versa)\n");

	fprintf(out, "         -t <1-%zu>%n", maxthreads, &i);
	// when col width increases, add spaces into the string as well
	fprintf(out, "%.*snumber of parallel threads to use\n", i>=29?0:29-i, "                             ");

	fprintf(out, "         -v                  verbose output\n"
	             "         -d                  debug mode (mainly pkg db syntax validation)\n"
	             "         -h                  display this overview\n"
	             "\n"
	             "Search indicated paths on disk and in the Portage pkg db and identify the\n"
	             "differences discovered.\n"
	             "\n"
	             "If the dirname of a path in an argument exists on disk, it is canonicalized\n"
	             "according to the filesystem, as the pkg db records all locations using canonical\n"
	             "paths. Symlinks in basenames are not followed. Paths with non-existing dirnames\n"
	             "must be absolute and have no \".\" or \"..\" components to be potentially searchable\n"
	             "in the pkg db. Exclusion (-e) paths follow the same rules.\n"
	             "\n"
	             "In every file the differences are provided using the characters:\n"
	             "'d' = dir, 'o' = reg. file, 's' = symlink, 'n' = other file type, '!' = missing,\n"
	             "'?' = unknown, '=' = matches, '#' = differs, ' ' = not applicable.\n"
	             "Five of them are given per each file. In order:\n"
	             "1. type of file on disk (d/o/s/n/!/?)\n"
	             "2. type of file in pkg db (d/o/s/!)\n"
	             "3. mtime verification (=/#/ )\n"
	             "4. hash verification (=/#/?/ )\n"
	             "5. symlink target verification (=/#/?/ )\n");

	flush_exit(code);
}



// the first char must be unique, use of pkgdb_filetype_str*[] is intentional,
// never rely on what the initial chars are
static inline const char *disk_filetype_str(const struct stat meta[static 1]) {
	switch (meta->st_mode & S_IFMT) {
		case S_IFREG: return pkgdb_filetype_str[pkgdb_obj];
		case S_IFDIR: return pkgdb_filetype_str[pkgdb_dir];
		case S_IFLNK: return pkgdb_filetype_str[pkgdb_sym];
		default: return "non-monitored type file";
	}
}

static inline const char *disk_filetype_struc(const struct stat meta[static 1]) {
	switch (meta->st_mode & S_IFMT) {
		case S_IFREG: return pkgdb_filetype_struc[pkgdb_obj];
		case S_IFDIR: return pkgdb_filetype_struc[pkgdb_dir];
		case S_IFLNK: return pkgdb_filetype_struc[pkgdb_sym];
		default: return "Non-monitored type file";
	}
}



static job *job_getblank(const char path[static 1], size_t pathlen) {
	assert(path[pathlen] == '\0');

	job *const newjob = malloc(sizeof *newjob);
	IFEX(! newjob);
	newjob->path = array_char_empty;
	IFEX(! copy_array_char(&newjob->path, path, pathlen+1));
	--newjob->path.len;  // don't count the \0
	return newjob;
}



static void job_push(job file[static 1]) {
	if (scanfs_args.nrthreads > 1) {
		pthread_mutex_lock(&jobsmx);
		IFEX(! append_array_ptr(&jobstack, (const void **)&file, 1));
		pthread_cond_signal(&jobsevent);
		pthread_mutex_unlock(&jobsmx);
	}
	else IFEX(! append_array_ptr(&jobstack, (const void **)&file, 1));
}



static void job_free(job file[static 1]) {
	free(file->path.a);
	free(file);
}



static inline int cmp_name_pkgdbfile(const void *name, const void *pkgdbfile) {
	assert(name && pkgdbfile && *(const pkgdb_file *const *)pkgdbfile);

	return strcmp((const char *)name, (*(const pkgdb_file *const *)pkgdbfile)->name);
}



static uint8_t job_do(const job file[static 1]) {
	assert(file->path.len && file->path.size > file->path.len);
	assert(! file->path.a[file->path.len]);
	assert(! file->oneshot);

	// TODO: what if it's dir on disk + not dir in pkgdb (or vice versa)?
	if (denylisted(file->path.a, file->path.len, file->ondisk==ondisk_ok && S_ISDIR(file->meta.st_mode) && file->pkgdbf && file->pkgdbf->type == pkgdb_dir))
		return EXIT_SUCCESS;

	job newjob;
	newjob.path = array_char_empty;
	uint8_t ret = EXIT_SUCCESS;

	// TODO: what does equery do when file is a sym, it is in pkgdbf, but not on
	// disk? does it tryfollow as well?
	// TODO: we might check a previously visited path
	if ( scanfs_args.tryfollow && ! file->pkgdbf &&
	     file->ondisk == ondisk_ok && S_ISLNK(file->meta.st_mode)
	   ) {
		// TODO: readlink_full() instead? For example,
		// "s!    /usr/share/man/man1/piconv.1.bz2" would be removed by that,
		// equery b does readlink()
		// ... but then we wouldn't report dead symlinks
		char *const newpath = canonicalize_file_name(file->path.a);
		if (newpath || (errno != ENOENT && errno != ENOTDIR)) {
			if (! newpath) {
				error(0, errno, "canonicalize_file_name(%s)", file->path.a);
				return mergeexits(ret, EXIT_PROGERROR);
			}
			// can't just newjob.path.a=newpath, allocated size isn't known
			IFEX(! copy_array_char(&newjob.path, newpath, strlen(newpath)+1));
			free(newpath);
			--newjob.path.len;  // don't count the \0
			if (! stat(newjob.path.a, &newjob.meta))
				newjob.ondisk = ondisk_ok;
			else {
				newjob.ondisk = ondisk_unknown;
				error(0, errno, "stat(%s)", newjob.path.a);
				ret = mergeexits(ret, EXIT_PROGERROR);
			}
			newjob.pkgdbf = pkgdb_get(&pkgdb_root, newjob.path.a);
			newjob.oneshot = true;
			file = &newjob;
		}
	}

	// [0] = type on disk, [1] = type in pkg db, [2] = timestamp, [3] = md5,
	// [4] = sym target
	// '!' = missing, '?' = couldn't find out, '=' = match, '#' = different,
	// ' ' - not applicable
	char tags[6] = "!!   ";
	if (file->ondisk == ondisk_ok)
		tags[0] = disk_filetype_str(&file->meta)[0];
	else if (file->ondisk == ondisk_unknown)
		tags[0] = '?';
	if (file->pkgdbf)
		tags[1] = pkgdb_filetype_str[file->pkgdbf->type][0];

	if (tags[1] != '!' && tags[0] == tags[1]) {
		switch (file->pkgdbf->type) {
		case pkgdb_dir:
			break;

		case pkgdb_obj:
			if (scanfs_args.verifymeta) {
				md5_arith_t hash[4];
				if (! md5(file->path.a, file->meta.st_blksize, hash)) {
					error(0, errno, "md5(%s)", file->path.a);
					ret = mergeexits(ret, EXIT_PROGERROR);
					tags[3] = '?';
				}
				else if (md5_equal(hash, file->pkgdbf->ext.obj.hash))
					tags[3] = '=';
				else tags[3] = '#';
				if (file->meta.st_mtime == file->pkgdbf->ext.obj.timestamp)
					tags[2] = '=';
				else tags[2] = '#';
			}
			break;

		case pkgdb_sym:
			assert(! file->oneshot);
			if (scanfs_args.verifymeta) {
				array_char s = array_char_empty;
				s.len = readlink_full(file->path.a, file->meta.st_size, &s.a, &s.size);
				if (s.len == (size_t)-1) {
					s.len = 0;
					error(0, errno, "readlink_full(%s)", file->path.a);
					ret = mergeexits(ret, EXIT_PROGERROR);
					tags[4] = '?';
				}
				else if ( s.len == file->pkgdbf->ext.sym.tgtlen &&
					      ! memcmp(s.a, file->pkgdbf->ext.sym.tgt, s.len)
					    ) {
						tags[4] = '=';
				}
				else tags[4] = '#';
				free(s.a);
				// POSIX 2004 doesn't guarantee st_mtime to be filled, but
				// there's no other way to get it. O_NOFOLLOW for open() isn't
				// supported.
				if (file->meta.st_mtime == file->pkgdbf->ext.sym.timestamp)
					tags[2] = '=';
				else tags[2] = '#';
			}
			break;

		case pkgdb_und:
			assert(file->pkgdbf->type);
			break;
		}
	}

	// the '?' cases have been signaled with error() (important!)
	if ( tags[1] == '!' || tags[0] != tags[1] || tags[3] == '#' ||
	     tags[2] == '#' || tags[4] == '#'
	   ) {
		printf("%s %s\n", tags, file->path.a);
		ret = mergeexits(ret, EXIT_SOMERESULTS);
	}
	// TODO: this is supposed to print the '?' cases as well, but some of those
	// will never reach here (e.g., tryfollow'd symlinks)
	else if (scanfs_args.verbose)
		printf("%s %s\n", tags, file->path.a);

	if ( file->oneshot ||
	     ( tags[0] != pkgdb_filetype_str[pkgdb_dir][0] &&
	       tags[1] != pkgdb_filetype_str[pkgdb_dir][0]
	     ) ||
	     (! scanfs_args.allmissing && tags[0] != tags[1])
	   ) {
		free(newjob.path.a);
		return ret;
	}

	job *newjobcopy;
	size_t dirnamelen;
	array_ptr pkgdb_dirlist = array_ptr_empty;
	// the character for tags[0] for files which are in pkgdb and aren't
	// remove()d during disk dir browse
	ondisk_status pkgdb_dirlist_ondisk = ondisk_missing;
	assert(file != &newjob);
	newjob.oneshot = false;
	ensalloc_array_char(&newjob.path, 128);

	if ( tags[1] == pkgdb_filetype_str[pkgdb_dir][0] &&
	     file->pkgdbf->ext.dir.contents
	   ) {
		pkgdb_dirlist = pkgdb_lsdir(file->pkgdbf);
		IFEX(! pkgdb_dirlist.len);
		if (tags[0] == '?')
			pkgdb_dirlist_ondisk = ondisk_unknown;
	}

	if (tags[0] == pkgdb_filetype_str[pkgdb_dir][0] || pkgdb_dirlist.len) {
		IFEX(! copy_array_char(&newjob.path, file->path.a, file->path.len + (file->path.a[file->path.len-1]!='/')));
		newjob.path.a[newjob.path.len-1] = '/';
		dirnamelen = newjob.path.len;
		// newjob.path isn't \0-term. now
	}

	// TODO: this type of use of pkgdb_filetype_str() is terrrible
	if (tags[0] == pkgdb_filetype_str[pkgdb_dir][0]) {
		void *const *pkgdb_dirlist_cur;
		bool isdir_disk, isdir_pkgdb;
		assert(sizeof (struct dirent) <= 10240);  // TODO: assumptions?
		struct dirent dirfile, *dirfile_next;

		DIR *const disk_dirlist = opendir(file->path.a);
		if (! disk_dirlist) {
			error(0, errno, "opendir(%s)", file->path.a);
			ret = mergeexits(ret, EXIT_PROGERROR);
			pkgdb_dirlist_ondisk = ondisk_unknown;
		}
		else {
			while (true) {
				// in POSIX 2004 readdir() is unsafe
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
				readdir_r(disk_dirlist, &dirfile, &dirfile_next);
#pragma GCC diagnostic pop
				if (! dirfile_next)
					break;
				if (isdotdir(dirfile.d_name))
					continue;

				newjob.path.len = dirnamelen;
				IFEX(! append_array_char(&newjob.path, dirfile.d_name, strlen(dirfile.d_name)+1));
				--newjob.path.len;
				if (! lstat(newjob.path.a, &newjob.meta))
					newjob.ondisk = ondisk_ok;
				else {
					newjob.ondisk = ondisk_unknown;
					error(0, errno, "lstat(%s)", newjob.path.a);
					ret = mergeexits(ret, EXIT_PROGERROR);
				}
				pkgdb_dirlist_cur = bsearch2(dirfile.d_name, pkgdb_dirlist.a, pkgdb_dirlist.len, sizeof(*pkgdb_dirlist.a), cmp_name_pkgdbfile);
				if (pkgdb_dirlist_cur) {
					newjob.pkgdbf = *pkgdb_dirlist_cur;
					remove_array_ptr(&pkgdb_dirlist, (pkgdb_dirlist_cur-pkgdb_dirlist.a)+1, 1);
				}
				else newjob.pkgdbf = NULL;
				if ( scanfs_args.samefs && newjob.ondisk == ondisk_ok &&
				     newjob.meta.st_dev != file->meta.st_dev
				   ) {
					continue;
				}

				// Ideally, only files which are known to be dirs on disk would
				// be pushed on jobstack (md5()ing is the greatest cpu hogger),
				// all others would be done right now. But this func can't
				// restart inside of itself and its recursion depth must be kept
				// <=1. On the upside, dirs in pkgdb which are non-existent or
				// without a known type on disk are rare.
				isdir_disk = newjob.ondisk == ondisk_ok && S_ISDIR(newjob.meta.st_mode);
				isdir_pkgdb = newjob.pkgdbf && newjob.pkgdbf->type == pkgdb_dir && newjob.pkgdbf->ext.dir.contents;
				if ( (scanfs_args.allmissing && (isdir_disk || isdir_pkgdb)) ||
				     (! scanfs_args.allmissing && isdir_disk && isdir_pkgdb)
				   ) {
					newjobcopy = job_getblank(newjob.path.a, newjob.path.len);
					// otherwise newjob.meta may be uninitialized
					if (newjob.ondisk == ondisk_ok)
						newjobcopy->meta = newjob.meta;
					newjobcopy->ondisk = newjob.ondisk;
					newjobcopy->oneshot = newjob.oneshot;
					newjobcopy->pkgdbf = newjob.pkgdbf;
					job_push(newjobcopy);
				}
				else ret = mergeexits(ret, job_do(&newjob));  // more likely
			}

			closedir(disk_dirlist);  // errors not interesting
		}
	}

	newjob.ondisk = pkgdb_dirlist_ondisk;
	assert(newjob.ondisk == ondisk_missing || newjob.ondisk == ondisk_unknown);
	for (size_t n=0; n<pkgdb_dirlist.len; ++n) {
		newjob.path.len = dirnamelen;
		IFEX(! append_array_char(&newjob.path, ((const pkgdb_file *)pkgdb_dirlist.a[n])->name, ((const pkgdb_file *)pkgdb_dirlist.a[n])->namelen+1));
		--newjob.path.len;
		newjob.pkgdbf = pkgdb_dirlist.a[n];

		// Ideally, all of these would be done right now, without the use of
		// jobstack. But this func can't restart inside of itself and its
		// recursion depth must be kept <=1. On the upside, this condition will
		// rarely be passed, because dirs in pkgdb which are non-existent or
		// without a known type on disk are rare.
		if ( scanfs_args.allmissing && newjob.pkgdbf->type == pkgdb_dir &&
		     newjob.pkgdbf->ext.dir.contents
		   ) {
			newjobcopy = job_getblank(newjob.path.a, newjob.path.len);
			// newjobcopy->meta uninitialized
			newjobcopy->ondisk = newjob.ondisk;
			newjobcopy->oneshot = newjob.oneshot;
			newjobcopy->pkgdbf = newjob.pkgdbf;
			job_push(newjobcopy);
		}
		else ret = mergeexits(ret, job_do(&newjob));  // more likely
	}

	free(pkgdb_dirlist.a);
	free(newjob.path.a);
	return ret;
}



// any kind of flie can be on jobstack
static void *job_get(void *exitcode_thr) {
	// Popping from jobstack and the increment of busythreads must take place
	// atomically. When there are no jobs on stack and no busy threads (which
	// may produce jobs in the future), exit.
	// jobsevent = there's a reason to stop waiting, which is a new job on stack
	// or a busythreads decrement.
	job *file;
	*(uint8_t *)exitcode_thr = EXIT_SUCCESS;

	if (scanfs_args.nrthreads > 1) {
		pthread_mutex_lock(&jobsmx);
		do {
			while (! jobstack.len && busythreads)
				pthread_cond_wait(&jobsevent, &jobsmx);
			if (jobstack.len) {
				file = jobstack.a[--jobstack.len];
				++busythreads;
				pthread_mutex_unlock(&jobsmx);
				*(uint8_t *)exitcode_thr = mergeexits(*(uint8_t *)exitcode_thr, job_do(file));
				job_free(file);
				pthread_mutex_lock(&jobsmx);
				--busythreads;
				pthread_cond_broadcast(&jobsevent);
			}
		} while (jobstack.len || busythreads);
		pthread_mutex_unlock(&jobsmx);
	}
	else while (jobstack.len) {
		file = jobstack.a[--jobstack.len];
		*(uint8_t *)exitcode_thr = mergeexits(*(uint8_t *)exitcode_thr, job_do(file));
		job_free(file);
	}

	return NULL;
}



int main(int argc, char *argv[]) {
	make_assumptions();
	funcs_compat_init(argv[0]);
	atexit(stdio_flush_last);
	setlocale(LC_ALL, "");
	// prevent stdout/err from interleaving
	assert(fitprod2zuzu(2, iosize(STDOUT_FILENO, -1)));
	assert(fitprod2zuzu(2, iosize(STDERR_FILENO, -1)));
	setvbuf(stdout, NULL, _IOLBF, 2 * iosize(STDOUT_FILENO, -1));
	setvbuf(stderr, NULL, _IOLBF, 2 * iosize(STDERR_FILENO, -1));

	uintmax_t maxthreads = sysconf(_SC_THREAD_THREADS_MAX);
	assert(LONG_MAX < UINTMAX_MAX);
	if (maxthreads == 0 || maxthreads == (uintmax_t)-1L)
#if PTHREAD_THREADS_MAX > 0  // requires <limits.h>
		maxthreads = PTHREAD_THREADS_MAX;
#else
		maxthreads = _POSIX_THREAD_THREADS_MAX;  // 64
#endif
	// make sure extrathreads[] can be allocated, main thread isn't included
	if (SIZE_MAX / sizeof(pthread_t) < maxthreads - 1)
		maxthreads = SIZE_MAX / sizeof(pthread_t) + 1;
	assert(maxthreads > 0 && maxthreads <= SIZE_MAX);

	int i;
	char *s;

	do {
		// the optional POSIXLY_CORRECT is an env var, not a def.
		// TODO: make getopt_long()
		switch (i = getopt(argc, argv, "e:xfmat:vdh")) {
		case 'e':
			// dirname only - allows user to ask about a symlink
			s = canonicalize_dirname(optarg);
			if (! s) {
				if (errno != ENOENT && errno != ENOTDIR) {
					error(0, errno, "Ignoring \"-e %s\"", optarg);
					exitcode = mergeexits(exitcode, EXIT_PROGERROR);
					break;
				}
				if (optarg[0] != '/' || hasdotdir(optarg))
					usage(EXIT_USERERROR, maxthreads);
				path_trimsl(optarg, false);
				s = optarg;
			}
			IFEX(! denylist_addordiscard(s, s!=optarg));
			break;
		case 'x':
			scanfs_args.samefs = true;
			break;
		case 'f':
			scanfs_args.tryfollow = true;
			break;
		case 'm':
			scanfs_args.verifymeta = true;
			break;
		case 'a':
			scanfs_args.allmissing = true;
			break;
		case 't':
			if (optarg[0] >= '0' && optarg[0] <= '9') {
				errno = 0;
				uintmax_t u = strtoumax(optarg, &s, 10);
				if (! errno && ! *s && u > 0 && u <= maxthreads) {
					scanfs_args.nrthreads = u;
					break;
				}
			}
			usage(EXIT_USERERROR, maxthreads);
			break;
		case 'v':
			pkgdb_verbose = scanfs_args.verbose = true;
			break;
		case 'd':
			pkgdb_debug = true;
			break;
		case 'h':
			usage(EXIT_SUCCESS, maxthreads);
			break;
		case '?':
			usage(EXIT_USERERROR, maxthreads);
			break;
		}
	} while (i != -1);
	if (optind == argc)  // any paths left?
		usage(EXIT_USERERROR, maxthreads);

	// uint8_t to make sure the total array size <= total size of extrathreads[]
	pthread_t *extrathreads = NULL;
	uint8_t mainthread_exit, *extrathreads_exit=NULL;
	pthread_attr_t thrattr;
	if (scanfs_args.nrthreads > 1) {
		pthread_mutexattr_t jobsmxattr;
		IFEX((errno = pthread_mutexattr_init(&jobsmxattr)));
		pthread_mutexattr_settype(&jobsmxattr, PTHREAD_MUTEX_NORMAL);
		if ((i = pthread_mutex_init(&jobsmx, &jobsmxattr))) {  // can be EPERM
			error(0, i, "pthread_mutex_init(PTHREAD_MUTEX_NORMAL)");
			exitcode = mergeexits(exitcode, EXIT_PROGERROR);
			scanfs_args.nrthreads = 1;
		}
		else {
			IFEX((errno = pthread_cond_init(&jobsevent, NULL)));
			IFEX((errno = pthread_attr_init(&thrattr)));
			pthread_attr_setdetachstate(&thrattr, PTHREAD_CREATE_DETACHED);
			extrathreads = malloc((scanfs_args.nrthreads-1) * sizeof(*extrathreads));
			IFEX(! extrathreads);
			extrathreads_exit = malloc((scanfs_args.nrthreads-1) * sizeof(*extrathreads_exit));
			IFEX(! extrathreads_exit);
		}
		// meaning pthread_mutex_lock() can't fail
		assert(! pthread_mutexattr_getprotocol(&jobsmxattr, &i) && i != PTHREAD_PRIO_PROTECT);
		pthread_mutexattr_destroy(&jobsmxattr);
	}

	if (! pkgdb_read_db())
		flush_exit(mergeexits(exitcode, EXIT_PROGERROR));

	// All paths and excludes: if path exists on disk, it is first canonicalized
	// and then used (scanned / excluded). if it doesn't exist, it must be
	// absolute and without "."/".." and is used verbatim. Paths with uncertain
	// existence on disk are discarded here (to make it clear what happens).
	// Note that in pkg db there is no concept of path canonicalization or
	// symlink following.
	ensalloc_array_ptr(&jobstack, 2048);
	job *startjob;
	bool known_ondisk_missing;
	// TODO: skip paths which are inside some other
	for (i=optind; i<argc; ++i) {
		known_ondisk_missing = false;
		s = canonicalize_dirname(argv[i]);
		if (! s) {
			if (errno != ENOENT && errno != ENOTDIR) {
				error(0, errno, "Skipping \"%s\"", argv[i]);
				exitcode = mergeexits(exitcode, EXIT_PROGERROR);
				continue;
			}
			if (argv[i][0] != '/' || hasdotdir(argv[i]))
				usage(EXIT_USERERROR, maxthreads);
			path_trimsl(argv[i], false);
			s = argv[i];
			known_ondisk_missing = true;
		}

		// can't use s in startjob->path, allocated size isn't known
		startjob = job_getblank(s, strlen(s));
		// non-existence will be reported using tags in job_do()
		if (known_ondisk_missing)
			startjob->ondisk = ondisk_missing;
		else if (! lstat(s, &startjob->meta))
			startjob->ondisk = ondisk_ok;
		else if (errno == ENOENT || errno == ENOTDIR)
			startjob->ondisk = ondisk_missing;
		else {
			startjob->ondisk = ondisk_unknown;
			error(0, errno, "lstat(%s)", argv[i]);
			exitcode = mergeexits(exitcode, EXIT_PROGERROR);
		}
		if (startjob->ondisk != ondisk_unknown) {
			startjob->pkgdbf = pkgdb_get(&pkgdb_root, s);
			startjob->oneshot = false;
			job_push(startjob);
		}
		else job_free(startjob);  // unlikely
		if (s != argv[i])
			free(s);
	}

	if (jobstack.len) {
		size_t n;
		assert(busythreads == 0);
		for (n=1; n<scanfs_args.nrthreads; ++n) {
			i = pthread_create(extrathreads+(n-1), &thrattr, job_get, extrathreads_exit+(n-1));
			if (i) {  // can be EPERM
				error(0, i, "pthread_create()");
				exitcode = mergeexits(exitcode, EXIT_PROGERROR);
				--n; --scanfs_args.nrthreads;
				if (scanfs_args.nrthreads == 1) {
					free(extrathreads_exit);
					free(extrathreads);
					// order of destroys!
					pthread_cond_destroy(&jobsevent);
					pthread_mutex_destroy(&jobsmx);
				}
			}
			if (n == scanfs_args.nrthreads - 1)  // in any case
				pthread_attr_destroy(&thrattr);
		}
		job_get(&mainthread_exit);
		assert(busythreads == 0);
		assert(jobstack.len == 0);
		exitcode = mergeexits(exitcode, mainthread_exit);
		for (n=1; n<scanfs_args.nrthreads; ++n)
			exitcode = mergeexits(exitcode, extrathreads_exit[n-1]);
	}

	flush_exit(exitcode);
}
