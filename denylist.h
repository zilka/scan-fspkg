/* Copyright 2023 Roman Žilka

   This file is part of Scan-fs.

   Scan-fs is free software: you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free Software
   Foundation, either version 2 of the License, or (at your option) any later
   version.

   Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   Scan-fs. If not, see <https://www.gnu.org/licenses/>. */



#ifndef DENYLIST_H_INCLUDED
#define DENYLIST_H_INCLUDED

struct denylist_entry {
	char *path;  // not \0-term, no trailing '/' (except for path="/")
	size_t pathlen;
	// true => only eliminates paths which are known to be dirs,
	// false => eliminates files of all types
	bool isdir;
	bool freeable;
};

#define TYPE struct denylist_entry
#define TYPESHORT dlentry
#define ARRAY_CUSTOM1
#include "array.h"



// non-thread-safe functions
bool denylist_addordiscard(char s[static 2], bool freeable);
void denylist_erase(void);

// thread-safe functions
bool denylisted(const char s[static 1], size_t len, bool s_known_to_be_dir);

#endif
