# Copyright 2023 Roman Žilka
#
# This file is part of Scan-fs.
#
# Scan-fs is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 2 of the License, or (at your option) any later
# version.
#
# Scan-fs is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Scan-fs. If not, see <https://www.gnu.org/licenses/>.



PROGRAM = scan-fs
OBJS ::= pkgdb.o denylist.o scan-fs.o
SRCS ::= $(OBJS:.o=.c)

LIBC99_DIR = ../libc99
LIBC99_CPPFLAGS ::= $(addprefix -I$(LIBC99_DIR)/,. array)
LIBC99_OBJS ::= $(addprefix $(LIBC99_DIR)/,funcs.o funcs-compat.o md5.o assumptions.o)



# $(shell ...) output assigned using ::= to prevent the external commands from
# executing more than once
BUILDSTAT_FILE = .buildstat
GIT_STATUS ::= $(shell git status -s 2>&1 | grep -v '^.. Makefile' | md5sum | cut -c 1-32)
# repo exists, clean, Makefile modification ignored
ifeq ($(GIT_STATUS),d41d8cd98f00b204e9800998ecf8427e)
GIT_TAG ::= $(shell git describe --exact-match 2>/dev/null | tr -dc 'a-zA-Z0-9_.-')
GIT_CDATE ::= $(shell git log --format=format:%cs HEAD^! 2>/dev/null)
ifeq ($(GIT_TAG),)
GIT_CHASH ::= $(shell git log --format=format:%h HEAD^! 2>/dev/null)
BUILDSTAT_NEW ::= (commit $(GIT_CHASH), $(GIT_CDATE))
else
BUILDSTAT_NEW ::= $(GIT_TAG) ($(GIT_CDATE))
endif
else
BUILDSTAT_NEW ::= (build out of sync with repo)
endif

# It's not optimal to invoke rebuild after every commit. When the status was
# "out of sync with repo" last time as well as now, it doesn't matter what the
# last commit is currently.
ifneq ($(file <$(BUILDSTAT_FILE)),$(BUILDSTAT_NEW))
$(file >$(BUILDSTAT_FILE),$(BUILDSTAT_NEW))
BUILDSTAT_NEEDREBUILD = dummy
endif



CC = gcc
CFLAGS ::= -std=c99 -Wall -Wextra -pedantic -pipe -pthread
CPPFLAGS ::= -D_XOPEN_SOURCE=600 $(LIBC99_CPPFLAGS) -DBUILDSTAT='$(BUILDSTAT_NEW)'
# as-needed must come after all source files and before -l*
# gc-sections makes improvements even without -f*-sections
LDFLAGS ::= -Wl,{-O3,--as-needed,--gc-sections,-z\,relro}
LIBS = -lpthread

# the thread sanitizer is incompatible with most other sanitizers
#CFLAGS_ANALYZER ::= -fanalyzer -fanalyzer-verbosity=0
#CFLAGS_ANALYZER += -fsanitize={address,pointer-compare,pointer-subtract,leak,undefined} -fsanitize-sections=\* -fsanitize-address-use-after-scope
#CFLAGS_ANALYZER += -fsanitize={thread,undefined} -fsanitize-sections=\* -fsanitize-address-use-after-scope

# production
CFLAGS += -O3 -march=native -s
CPPFLAGS += -DNDEBUG
LDFLAGS += -Wl,--relax
# assembler inspection
#CFLAGS += -O3
#CPPFLAGS += -DNDEBUG
#LDFLAGS += -Wl,--relax
# profiling
#CFLAGS += -O3 -g3 -pg
#CPPFLAGS += -DNDEBUG
# development Og
#CFLAGS += -Og -g3 -march=native $(CFLAGS_ANALYZER)
#CPPFLAGS += -D_FORTIFY_SOURCE=3
# development O0
#CFLAGS += -g3 -march=native $(CFLAGS_ANALYZER)



.PHONY: libc99 clean $(BUILDSTAT_NEEDREBUILD)

$(PROGRAM): $(OBJS) $(LIBC99_OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LIBS)

# Does $CC execute, do *.c exist (regardless of whether we have *.o)?
# The subst corrects multi-line output of gcc -MM.
# Eval necessary, because foreach removes all newlines. So does $(shell).
define GENDEPS =
RULE ::= $$(subst \ , ,$$(shell $(CC) -MM $(CPPFLAGS) $(1) 2>/dev/null))
DEPSDONE += $$(firstword $$(RULE))
$$(RULE)
endef
DEPSDONE ::=
$(foreach SRC,$(SRCS),$(eval $(call GENDEPS,$(SRC))))
ifneq ($(DEPSDONE::=),$(OBJS))
$(error Cannot run $(CC) -MM $(SRCS))
endif

# scan-fs.o contains the commit info
scan-fs.o: $(BUILDSTAT_NEEDREBUILD)
# If Makefile changes, everything must be rebuilt.
$(OBJS): Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $(@:.o=.c)
# suppress default recipe
$(LIBC99_OBJS): libc99 ;

libc99:
	$(MAKE) -C $(LIBC99_DIR)

clean:
	rm -f $(OBJS) $(PROGRAM)
